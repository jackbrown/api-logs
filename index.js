var fs = require("fs");
var path = require('path')
var crypto = require('crypto');
var FOLDER = process.env.NODE_LOG_FOLDER;
if (FOLDER==undefined || FOLDER=='undefined'){
    console.log("Configure la carpeta de destino de los ficheros `export NODE_LOG_FOLDER= <path>`");
    return;
}

function getRequestString(struct) {
    var template = `
    var request = require("request");
    var util=require("util")
    var options = { method: '${struct.method.toUpperCase()}',
    url: '${struct.protocol}://${struct.host}${struct.path}',
    qs: ${JSON.stringify(struct.query)},
    headers: ${JSON.stringify(struct.headers)},
    body: ${JSON.stringify(struct.body)},
    json: true };
	delete options.headers['accept-encoding'];
    request(options, function (error, response, body) {
    if (error) throw new Error(error);

    console.log(util.inspect(body,{depth:null}));
    });
    `;
    return template;
}

exports.hello=function(){
    console.log("\n\n\nHOLA DESDE EL INSPECTOR\n\n\n")
}

exports.start = function (req, res, next) {
    var struct = {
        headers: {},
        method: req.method,
        path: req.path,
        query: {},
        body: {},
        url: req.url,
        ip: req.ip,
        protocol: req.protocol,
        hostname: req.hostname,
        host: req.headers.host
    }
    if (req.headers) {
        struct.headers = JSON.parse(JSON.stringify(req.headers));
        delete struct.headers["content-length"];
        delete struct.headers["if-none-match"];
    }
    if (req.query) {
        struct.query = JSON.parse(JSON.stringify(req.query))
    }
    if (req.body) {
        struct.body = JSON.parse(JSON.stringify(req.body))
    }
    // return res.sendStatus(204);
    res.on('finish', function onResFinish() {
        if (res.statusCode == 500
            || res.statusCode == 400
            || res.statusCode == 403
            || res.statusCode == 401
            || res.statusCode == 200
            //~ || res.statusCode == 304
            ||req.method=='POST'
            ||req.method=='PUT'
            //~ ||req.method=='GET'
            ||req.method=='PATCH'
        ) {
            processOnFinish(struct, req, res)
        }
    });
    return next();
}

function getUnique(struct){
    var sha256 = crypto.createHash('sha256').update(struct).digest('hex');
    return sha256;
}
function processOnFinish(struct, req, res) {
    console.log("Executing...")
    struct = getRequestString(struct)
    var unique = getUnique(struct);
    
    var encontrado = false;
    var list = fs.readdirSync(FOLDER)
    for (var i = 0; i < list.length && !encontrado; i++) {
        if (list[i].indexOf(unique) != -1) {
            encontrado = true;
        }
    }
    if (!encontrado) {
        console.log("salvando")
        var url = req.url.split("/").join("-").split("?")[0];
        var a = new Date()
        fs.writeFileSync([FOLDER, a.toISOString().split(":").join("-") + unique + '-' + res.statusCode + '-' + req.method + "-" + url + ".js"].join(path.sep), struct, 'utf8')       
    }else{
        console.log("Ya existia :)")
    }
   
}

