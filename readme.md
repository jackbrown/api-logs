sh run.sh

export NODE_LOG_FILE=<path to node_modules/logs-api>
export NODE_LOG_FOLDER=<path to node_modules/logs-api/datas>

requerir en el app luego de parsear el body, antes de la gestion de las urls (no almacena las peticiones con file directo)


try {
  var nconf = require('nconf');
  nconf
    .argv()
    .env('_');
  var folderLogsFile = nconf.get('NODE:LOG:FILE');
  if (folderLogsFile == undefined || folderLogsFile=='') {
    console.log("Configure la carpeta de destino de los ficheros `export NODE_LOG_FOLDER= <path>`");
    return;
  }
	var logsApi = require(folderLogsFile);
	logsApi.hello();
	app.use(logsApi.start)

} catch (e) {
  console.log("No esta usando la gestion de logs de los requests...")
}
